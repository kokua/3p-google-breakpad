#!/bin/sh
#As of this writing, google breakpad does not have a version number.
#We therefor will use the SVN checkout revision of the latest checkout from google:
#       svn info | grep "Revision" | awk '{print $2}'
#The following VERSION string will need to be updated manually.
version=1413		#svn revision as of 2014-10-29

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

if [ -z "$AUTOBUILD" ] ; then
    fail
fi

# load autobuild provided shell functions and variables
# first remap the autobuild env to fix the path for sickwin
if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)"

LIBRARY_DIRECTORY_DEBUG=./stage/lib/debug
LIBRARY_DIRECTORY_RELEASE=./stage/lib/release
BINARY_DIRECTORY=./stage/bin
INCLUDE_DIRECTORY=./stage/include/google_breakpad
mkdir -p "$LIBRARY_DIRECTORY_DEBUG"
mkdir -p "$LIBRARY_DIRECTORY_RELEASE"
mkdir -p "$BINARY_DIRECTORY"
mkdir -p "$INCLUDE_DIRECTORY"
mkdir -p "$INCLUDE_DIRECTORY/common"

build_linux()
{
        if [ -f /usr/bin/gcc-4.6 ] ; then
            export CC=gcc-4.6
        fi
        if [ -f /usr/bin/g++-4.6 ] ; then
            export CXX=g++-4.6
        fi
        mkdir -p "$INCLUDE_DIRECTORY/client/linux/dump_writer_common"
        cp src/client/linux/dump_writer_common/*.h "$INCLUDE_DIRECTORY/client/linux/dump_writer_common/"
        VIEWER_FLAGS="-m$1 -fno-stack-protector"
        ./configure --prefix="$stage"  CFLAGS="$VIEWER_FLAGS" CXXFLAGS="$VIEWER_FLAGS" LDFLAGS=-m$1 
        make
        make -C src/tools/linux/dump_syms/ dump_syms CXXFLAGS="-g3 -O2 -Wall -m$1"
        make install
        mkdir -p "$INCLUDE_DIRECTORY/common"
        mkdir -p "$INCLUDE_DIRECTORY/google_breakpad/common"
        mkdir -p "$INCLUDE_DIRECTORY/client/linux/handler"
        mkdir -p "$INCLUDE_DIRECTORY/client/linux/crash_generation"
        mkdir -p "$INCLUDE_DIRECTORY/client/linux/minidump_writer"
        mkdir -p "$INCLUDE_DIRECTORY/client/linux/log"
        mkdir -p "$INCLUDE_DIRECTORY/third_party/lss"


#        cp -P $stage/lib/libbreakpad*.so* "$LIBRARY_DIRECTORY_RELEASE"
        cp src/client/linux/handler/*.h "$INCLUDE_DIRECTORY"
        cp src/client/linux/crash_generation/*.h "$INCLUDE_DIRECTORY/client/linux/crash_generation"
        mkdir -p "$INCLUDE_DIRECTORY/processor"
        cp src/processor/scoped_ptr.*h "$INCLUDE_DIRECTORY/processor"
        cp src/tools/linux/dump_syms/dump_syms "$BINARY_DIRECTORY"
    # replicate breakpad headers
        cp src/common/*.h "$INCLUDE_DIRECTORY/common"
        cp src/google_breakpad/common/*.h "$INCLUDE_DIRECTORY/google_breakpad/common"

    # no really all of them
        cp src/client/linux/minidump_writer/*.h "$INCLUDE_DIRECTORY/client/linux/minidump_writer/"
        cp src/client/linux/handler/*.h "$INCLUDE_DIRECTORY/client/linux/handler/"
        cp src/client/linux/log/*.h "$INCLUDE_DIRECTORY/client/linux/log/"
        cp src/third_party/lss/* "$INCLUDE_DIRECTORY/third_party/lss/"

    # and then cherry-pick some so they are found as used by linden
        cp src/common/using_std_string.h "$INCLUDE_DIRECTORY"
        cp src/client/linux/handler/exception_handler.h "$INCLUDE_DIRECTORY/google_breakpad/"
        cp src/client/linux/handler/minidump_descriptor.h "$INCLUDE_DIRECTORY/google_breakpad/"

    # libs and binaries
        cp -P $stage/lib/libbreakpad*.a* "$LIBRARY_DIRECTORY_RELEASE"

        pwd
        make clean
#        pushd src/tools/linux/dump_syms/
#        make clean
#        popd
}


case "$AUTOBUILD_PLATFORM" in
    "windows")
        # patch vcproj generator to use Multi-Threaded DLL for +3 link karma
        #
	# modified gyp is checked in now...patch is in repo for reference
	#
        # patch -p 1 < gyp.patch
        (
            cd src/client/windows
            ../../tools/gyp/gyp --no-circular-check -f msvs -G msvs-version=2013
        )

        load_vsvars

        devenv.com src/client/windows/breakpad_client.sln /Upgrade
        devenv.com src/tools/windows/dump_syms/dump_syms.vcproj /Upgrade

        devenv.com src/client/windows/breakpad_client.sln /build "release" /project exception_handler
        devenv.com src/client/windows/breakpad_client.sln /build "debug" /project exception_handler
        devenv.com src/client/windows/breakpad_client.sln /build "release" /project crash_generation_client
        devenv.com src/client/windows/breakpad_client.sln /build "debug"  /project crash_generation_client
        devenv.com src/client/windows/breakpad_client.sln /build "release" /project crash_generation_server
        devenv.com src/client/windows/breakpad_client.sln /build "debug"  /project crash_generation_server
        devenv.com src/client/windows/breakpad_client.sln /build "release"  /project common
        devenv.com src/client/windows/breakpad_client.sln /build "debug"  /project common

        #using devenv directly - buildconsole doesn't support building vs2010 vcxproj files directly, yet
        devenv.com src/tools/windows/dump_syms/dump_syms.vcxproj /build "release|win32"

        mkdir -p "$INCLUDE_DIRECTORY/client/windows/"{common,crash_generation}
        mkdir -p "$INCLUDE_DIRECTORY/common/windows"
        mkdir -p "$INCLUDE_DIRECTORY/google_breakpad/common"
        mkdir -p "$INCLUDE_DIRECTORY/processor"

        cp ./src/client/windows/handler/exception_handler.h "$INCLUDE_DIRECTORY"
        cp ./src/client/windows/common/*.h "$INCLUDE_DIRECTORY/client/windows/common"
        cp ./src/common/windows/*.h "$INCLUDE_DIRECTORY/common/windows"
        cp ./src/client/windows/crash_generation/*.h "$INCLUDE_DIRECTORY/client/windows/crash_generation"
        cp ./src/google_breakpad/common/*.h "$INCLUDE_DIRECTORY/google_breakpad/common"
        cp ./src/client/windows/Debug/lib/*.lib "$LIBRARY_DIRECTORY_DEBUG"
        cp ./src/client/windows/Release/lib/*.lib "$LIBRARY_DIRECTORY_RELEASE"
        cp ./src/tools/windows/dump_syms/Release/dump_syms.exe "$BINARY_DIRECTORY"
        cp src/processor/scoped_ptr.h "$INCLUDE_DIRECTORY/processor/scoped_ptr.h"
        cp src/common/scoped_ptr.h "$INCLUDE_DIRECTORY/common/scoped_ptr.h"
    ;;
    darwin)
        (
            cd src/
	    rm CMakeCache.txt
            cmake -G Xcode CMakeLists.txt
            xcodebuild -arch i386 -project google_breakpad.xcodeproj -configuration Release
        )
        xcodebuild -arch i386 -project src/tools/mac/dump_syms/dump_syms.xcodeproj MACOSX_DEPLOYMENT_TARGET=10.7 -configuration Release
        mkdir -p "$INCLUDE_DIRECTORY/processor"
        mkdir -p "$INCLUDE_DIRECTORY/google_breakpad/common"
        mkdir -p "$INCLUDE_DIRECTORY/client/mac/crash_generation"
        mkdir -p "$INCLUDE_DIRECTORY/client/mac/crash_generation/common/mac"
        mkdir -p "$INCLUDE_DIRECTORY/client/mac/handler"
        cp ./src/client/mac/handler/exception_handler.h "$INCLUDE_DIRECTORY"
        cp ./src/client/mac/handler/ucontext_compat.h "$INCLUDE_DIRECTORY/client/mac/handler"
        cp ./src/client/mac/crash_generation/crash_generation_client.h "$INCLUDE_DIRECTORY/client/mac/crash_generation"
        cp ./src/common/mac/MachIPC.h "$INCLUDE_DIRECTORY/client/mac/crash_generation/common/mac"
        cp ./src/client/mac/handler/Release/libexception_handler.dylib "$LIBRARY_DIRECTORY_RELEASE"
        cp ./src/tools/mac/dump_syms/build/Release/dump_syms "$BINARY_DIRECTORY"
        cp src/processor/scoped_ptr.h "$INCLUDE_DIRECTORY/processor/scoped_ptr.h"
        cp src/common/scoped_ptr.h "$INCLUDE_DIRECTORY/common/scoped_ptr.h"
    ;;
    linux)
        build_linux 32
    ;;
    linux64)
        build_linux 64
    ;;

esac

#Write phony version number into version file.
build=${AUTOBUILD_BUILD_ID:=0}
echo "${version}.${build}" > "${stage}/stage/version.txt"

# yes, this looks dumb, no, it's not incorrect
mkdir -p ${stage}/stage/LICENSES
cp COPYING ${stage}/stage/LICENSES/google_breakpad.txt

pass

